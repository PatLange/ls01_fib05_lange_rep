﻿import java.util.Scanner;

class Fahrkartenautomat {
	
	public static double zuZahlenderBetrag = 0.0;
	public static double eingezahlterGesamtbetrag = 0.0;
	public static double ticketAnzahl = 0.0;
	public static Scanner tastatur = new Scanner(System.in);
	
	
	
	public static void main(String[] args) {
        fahrkartenbestellungErfassen ();
	    fahrkartenBezahlen (zuZahlenderBetrag);
	    fahrkartenAusgeben (ticketAnzahl);
	    rueckgeldAusgeben (eingezahlterGesamtbetrag, zuZahlenderBetrag);
       
       
        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");
    }
	
	public static void fahrkartenbestellungErfassen () {
		String[] ticketArten = {"(0) Einzelfahrausweis Regeltarif AB\n", "(1) Einzelfahrausweis Ermäßigungstarif AB\n", "(2) Tageskarte Regeltarif AB\n", "(3) Tageskarte Ermäßigungstarif AB\n"};
		double[] preisListe = {2.90, 1.80, 8.60, 5,50};
		int input = 0;
		
		System.out.print("Anzahl der Tickets: ");
		ticketAnzahl = tastatur.nextDouble();
       
		for (double i = ticketAnzahl; i > 0; i--) {
			for(String ticketArt : ticketArten) {
				   System.out.print(ticketArt);
				}
			System.out.print("Art des Tickets:\n");
			input = tastatur.nextInt();
			zuZahlenderBetrag += preisListe[input];
		}
	}
	
	// Die Anzahl der Tickets wird hier vor der Ticketart abgefragen, da dies einen kompakteren Code bietet, die 
	// Zusammenstellung der Tickets komplett individuell ist und Kunden am Automaten meist eine überschaubare Menge 
	// an Tickets kaufen.
	
	public static void fahrkartenBezahlen (double zuZahlen) {
		double eingeworfeneMünze;
	       	       
		while(eingezahlterGesamtbetrag < zuZahlen) {
			System.out.printf("Noch zu zahlen: " + (String.format("%.2f", zuZahlen - eingezahlterGesamtbetrag)) + " Euro\n");
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
    }
	
	public static void fahrkartenAusgeben (double anzahl) {
		if (anzahl > 1) {
	    	   System.out.println("\nFahrscheine werden ausgegeben");
	       }
	       else {
	    	   System.out.println("\nFahrschein wird ausgegeben");
	       }
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
       }
       System.out.println("\n\n");
	}
	
	public static void rueckgeldAusgeben (double eingezahlterBetrag, double zuZahlen) {
		double rückgabebetrag;
		
		rückgabebetrag = eingezahlterBetrag - zuZahlen;
		
	       if(rückgabebetrag > 0.0) {
	    	   System.out.println("Der Rückgabebetrag in Höhe von " + (String.format("%.2f", rückgabebetrag)) + " EURO");
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          rückgabebetrag = Math.round((rückgabebetrag - 2.0)*100D)/100D;
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          rückgabebetrag = Math.round((rückgabebetrag - 1.0)*100D)/100D;
	           }
	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rückgabebetrag = Math.round((rückgabebetrag - 0.5)*100D)/100D;
	           }
	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rückgabebetrag = Math.round((rückgabebetrag - 0.2)*100D)/100D;
	           }
	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rückgabebetrag = Math.round((rückgabebetrag - 0.1)*100D)/100D;
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen 
	           {
	        	  System.out.println("5 CENT");
	 	          rückgabebetrag = Math.round((rückgabebetrag - 0.05)*100D)/100D;
	           }
	      }
	}
}