
public class Aufgabe3 {

	 public static void main(String[] args) {
		System.out.printf("%-12s" , "Fahrenheit");
		System.out.printf("|");
		System.out.printf("%10s", "Celsius");
		System.out.printf("\n%-24s", "------------------------");
		System.out.printf("\n%-12s", "-20");
		System.out.printf("|");
		System.out.printf("%10s", "-28.89");
		System.out.printf("\n%-12s", "-10");
		System.out.printf("|");
		System.out.printf("%10s" , "-23.33");
		System.out.printf("\n%-12s", "+0");
		System.out.printf("|");
		System.out.printf("%10s", "-17.78");
		System.out.printf("\n%-12s", "+20");
		System.out.printf("|");
		System.out.printf("%10s", "-6.67");
		System.out.printf("\n%-12s", "+30");
		System.out.printf("|");
		System.out.printf("%10s", "-1.11");
	 }
}
